'use strict';

angular
  .module('peachtreeApp', ['ui.router', 'ngCookies', 'Authentication', 'TransactionsModule'])

  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    console.log('app.js - config');
    $stateProvider
      .state("login", {
        url: "/login",
        templateUrl: "auth/login/login.html",
        controller: "LoginController"
      })

      .state("register", {
        url: "/register",
        templateUrl: "auth/register/register.html",
        controller: "RegisterController"
      })

      .state("home", {
        templateUrl: "home/home.html",
      })

      $urlRouterProvider.otherwise('/login');
  }
  ])
  // .run([
  //   '$rootScope',
  //   '$location', 
  //   '$cookies', 
  //   '$http',
  //   function ($rootScope, $location, $cookies, $http) {
  //     // keep user logged in after page refresh
  //     $rootScope.globals = $cookies.getObject('globals') || {};
  //     if ($rootScope.globals.currentUser) {
  //       $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; 
  //       // $location.path('/transactions')
  //       console.log("Persistent log-in worked.")
  //     }
  //    // redirect to login page if not logged in
  //      else if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
  //         $location.path('/login');
  //       }

  //   }]);
