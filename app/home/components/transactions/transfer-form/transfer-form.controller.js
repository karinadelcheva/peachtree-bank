angular
    .module('TransactionsModule')
    .controller('transferFormController', ['transactionService', '$scope', '$filter', function (transactionService, $scope, $filter) {
        let ctrl = this;
        console.log("Transfer form controller reached");

        // generate transaction ID
        let getRandomInt = function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        };

        // create transaction model
        ctrl.transaction = {
            'recipient': 'Konstantin Ivanov',
            'amount': 50,
        };

        // format transfer date to match existing date format
        ctrl.formatDate = function () {
            var dateObj = new Date(ctrl.transaction.date);
            formattedDate = $filter('date')(dateObj, 'dd/MM/yyyy');
        }

        // format transaction.amount input into currency
        ctrl.formatAmount = function () {
            formattedAmount = ctrl.transaction.amount.toFixed(2);
        }

        // Ask confirmation if amount exceeds $100.00
        ctrl.validate = function () {
            if (ctrl.transaction.amount > 100) {
                let agree = confirm(`Are you sure you want to send $${ctrl.transaction.amount}.00?`);
                if (agree === true ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }

        // send transaction 
        ctrl.sendMoney = function sendMoney(transferForm) {
            ctrl.formatDate();
            ctrl.formatAmount();
            
            if (ctrl.validate()) {
                let transaction = {
                    'id': getRandomInt(6, 50),
                    "recipient": ctrl.transaction.recipient,
                    "sender": ctrl.transaction.account,
                    "amount": `$${formattedAmount}`,
                    "date": formattedDate,
                    "status": "sent"
                };

                transactionService.sendTransaction(transaction);
            }

        };
    }]);