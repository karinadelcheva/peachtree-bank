angular
.module('TransactionsModule')

.component('transferFormComponent', {
    templateUrl: "home/components/transactions/transfer-form/transfer-form.html",
    controller: 'transferFormController'
  })

.config(function($stateProvider) {
    $stateProvider
    .state(
        'transferForm',{
        // url: "/transfer",
        component: 'transferFormComponent',
        parent: 'home' 
        })
})