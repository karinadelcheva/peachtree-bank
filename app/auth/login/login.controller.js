'use strict';
 
angular.
module('Authentication')
 
.controller('LoginController',
    ['$scope', 'AuthenticationService', '$state', 
    function ($scope, AuthenticationService, $state, ) {
        
        // reset login status
        $scope.logout = function () {
        AuthenticationService.logout();
        console.log('Log-out successful')
        $state.go('login');
        };

        // Log in
        $scope.login = async function () {
            $scope.dataLoading = true;
            
            let user = {
                email: await $scope.email,
                password: await $scope.password
            }

            AuthenticationService.login(user.email, user.password)
            
            .then(function (response) {
                if(response) {
                    console.log('Authentication successful')
                    $state.go('home.transactions-list');
                } 
                else {
                    $scope.error = AuthenticationService.message;
                    $scope.dataLoading = false;
                }
            });
        };
    }]);