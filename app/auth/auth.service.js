'use strict';

angular
    .module('Authentication', ['firebase', 'ui.router'])

    .config(function ($firebaseRefProvider) {

        var config = {
            apiKey: "AIzaSyCb6b4qTGqQmjLgjIx78q2Sb3ciaOG8TWg",
            authDomain: "peachtreeapp-vlc.firebaseapp.com",
            databaseURL: "https://peachtreeapp-vlc.firebaseio.com",
            projectId: "peachtreeapp-vlc",
            storageBucket: "peachtreeapp-vlc.appspot.com",
            messagingSenderId: "278992529138"
        };

        $firebaseRefProvider
            .registerUrl({
                default: config.databaseURL,
                transactions: config.databaseURL + '/transactions'
            });

        firebase.initializeApp(config);
    })

    .service('AuthenticationService',
        ['$firebaseAuth', '$state', 'firebase',
            function ($firebaseAuth, $state, firebase) {
                let auth = firebase.auth();

                let service = {
                    auth: auth,
                    register: register,
                    login: login,
                    logout: logout,
                    isLoggedIn: isLoggedIn,
                };

                return service;

                // TODO: Error messages!
                function register(email, password) {
                    return $firebaseAuth().$createUserWithEmailAndPassword(email, password)
                        .catch(function (error) {
                            let errorCode = error.code;
                            let errorMessage = error.message;
                            if (errorCode == 'auth/email-already-in-use') {
                                alert('Looks like this email is already in use.');
                            } else {
                                alert(errorMessage);
                            }
                        });
                }


                function login(email, password) {
                    register
                    return $firebaseAuth().$signInWithEmailAndPassword(email, password)
                        .catch(function (error) {
                            let errorCode = error.code;
                            let errorMessage = error.message;
                            switch (errorCode) {
                                case errorCode = 'auth/user-not-found':
                                    alert("User can't be found. Try signing up.");
                                    break;
                                case errorCode = 'auth/wrong-password':
                                    alert("Wrong password");
                                    break;
                                default:
                                    alert(errorMessage);
                                    break;
                            }
                        });

                }


                function logout() {
                    auth.signOut().then(function () {
                        $state.go('login');
                    }).catch(function (error) {
                        let errorCode = error.code;
                        alert(errorCode + ': Oh, no! Something went wrong.')
                    });
                }



                function isLoggedIn() {
                    return auth.$getAuth();
                }




                // service.SetCredentials = function (email, password) {
                //     var authdata = Base64.encode(email + ':' + password);

                //     $rootScope.globals = {
                //         currentUser: {
                //             email: email,
                //             authdata: authdata
                //         }
                //     };

                //     $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
                //     $cookies.putObject('globals', $rootScope.globals);
                // };
            }]);