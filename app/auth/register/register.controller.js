'use strict';
 
angular.
module('Authentication')
 
.controller('RegisterController',
    ['$scope', 'AuthenticationService', '$state', 
    function ($scope, AuthenticationService, $state, ) {
        
        // Create user
        $scope.register = async function () {
            $scope.dataLoading = true;
            
            let user = {
                email: await $scope.email,
                password: await $scope.password
            }

            AuthenticationService.register(user.email, user.password)
            
            .then(function (response) {
                if(response) {
                    console.log('Registration successful')
                    $state.go('home.transactions-list');
                } 
                else {
                    $scope.error = AuthenticationService.message;
                    $scope.dataLoading = false;
                }
            });
        };
    }]);