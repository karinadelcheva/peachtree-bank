Peachtree Bank


Simple web app built on AngularJS.

1. Download the app/ clone the repo.
2. Run "npm start" in your console.
3. Login using email: test@email.com / pass: test

Explore the capabilities of the program, or use it as a skeleton for your own 
project.